﻿using System;
using System.Threading.Tasks;
using Todo.Data;
using Todo.Models;
using Xamarin.Forms;

namespace Todo.Views
{
    public partial class TodoListPage : ContentPage
    {
        public TodoListPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ShowAll();
        }

        private async Task ShowAll()
        {
            var database = await TodoItemDatabase.Instance;
            listView.ItemsSource = await database.GetItemsAsync();
        }

        async void OnItemAdded(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TodoItemPage
            {
                BindingContext = new TodoItem()
            });
        }

        async void OnFilterItemsClicked(object sender, EventArgs e)
        {
            var database = await TodoItemDatabase.Instance;
            listView.ItemsSource = await database.GetItemsNotDoneAsync();
        }

        async void OnShowAllClicked(object sender, EventArgs e)
        {
            await ShowAll();
        }

        async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushModalAsync(new TodoItemPage
                {
                    BindingContext = e.SelectedItem as TodoItem
                });
            }
        }
    }
}